---
title: "Programm Strukturtreffen"
date: 2018-10-28T15:45:21+01:00
draft: false
---
## Programm

|   Tag   | Zeit        |            Programmpunkt            |                 Ort                |
|:-------:|-------------|:-----------------------------------:|:----------------------------------:|
| Freitag |             |                                     |                                    |
|         | ...-16      | Anreise                             |                                    |
|         | 16-17:30    | Begrüßung/Kennenlernen              |              Gebäude 106, S25 (UK) |
|         | 17:30-19:30 | Weihnachtsmarkt                     |                                (K) |
|         | 19:30-...   | Abendessen                          |                                TBA |
| Samstag | 8:00        | Abfahrt nach Düsseldorf             |                        Bahnhof (K) |
|         | 9:30-12:00  | Ideenwerkstadt "Schule  von morgen" | Freie Christliche Gesamtschule (D) |
|         | 12-14       | Mittagessen/Landtag                 |                                (D) |
|         | 16-18       | Diskussion "Schule von  morgen"     |           Gebäude 100, HS XIA (UK) |
|         | 18:30-...   | Abendessen                          |                                TBA |
| Sonntag | 10-11       | Frühstück                           |       Kreisgeschäftsstelle FDP (K) |
|         | 11-13       | AKB 2019                            |       Kreisgeschäftsstelle FDP (K) |
|         | 13-...      | Abreise                             |                                    |

Das ausführliche Programm als .pdf-Datei, zusammen mit allen weiteren Informationen, findet ihr [hier](/programm.pdf)
