---
title: "Allgemeine Infos Strukturtreffen"
date: 2018-10-28T15:50:54+01:00
draft: false
---


## Anmeldung

Zur Anmeldung schreibt ihr bitte bis zum **30.10.2018** eine Mail an [bildung@stipendiat.org](mailto:bildung@stipendiat.org). Fügt der bitten hinzu, ob ihr

  * ein Semesterticket o.ä. für NRW habt.
  * Übernachtungen braucht.

Solltet ihr weitere Vorschläge/Wünsche/Anregungen o.ä. haben, freuen wir uns per Mail darüber!

## Unterkunft
Wenn sich genügend Teilnehmer, die eine Unterkunft benötigen, anmelden, versuchen wir, eine gemeinsame Unterkunft zu organisieren. Nähres dazu anfang November.

## Teilnahmekosten
Für die Teilnahme entstehen an sich keine Kosten. Wir rechnen aber mit

  *  etwa 10€ für die Zugfahrt,
  *  etwa 60€ für die beiden Übernachtungen

Wie viel Geld wir für Essen über das Wochenende ausgeben, liegt natürlich an uns.


## Fahrtkostenabrechnung
Die Fahrtkostenabrechnung finden (bei vollständiger Teilnahme) über die FNF statt. Vermutlich kann man bis zu 130€ Fahrkosten abrechnen.
